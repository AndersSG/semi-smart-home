/* The Semi-Smart Home
 *    2018-12-27
 *  Andreas Högberg
 *  
 *  The codebase for the 
 * semi-smart home project
 */

#include "U8glib.h"

// Shapes for snowflake object : 1-7 = 1px, 8-15 = 2x2px, 15-20 = star 
typedef struct flakeStruct {
  float xPos;
  float yPos;
  float shape;
  float vel;
} Flake;

// Create new SSD1306 I2C display with 128x64 resolution
U8GLIB_SSD1306_128X64 oled(U8G_I2C_OPT_NONE);

const int section_one_red = 6, section_one_green = 7;
const int section_two_red = 4, section_two_green = 3;
const int string_lights = 8;
const int amount_of_flakes = 40;

char receivedChar;

boolean isNewData = false, tvOn = false, stringLightsOn = false;

// Array of snowflakes to simulate on tv-screen
Flake snow[amount_of_flakes];

void setup() {
  screen_setup();
  
  pinMode(section_one_red, OUTPUT);
  pinMode(section_one_green, OUTPUT);

  pinMode(section_two_red, OUTPUT);
  pinMode(section_two_green, OUTPUT);

  pinMode(string_lights, OUTPUT);

  Serial.begin(9600);
  Serial.print(" --- Is Ready --- ");
}

void loop() {
  oled.firstPage();
  
  if (tvOn) {
    screen_loop();
  }
  
  recieve_one_char();
  show_new_data();
}

// When character is received, read data then handle it
void recieve_one_char() {
  if (Serial.available() > 0) {
    receivedChar = Serial.read();
    isNewData = true;

    handle_data(toUpper(receivedChar);
  }
}

void handle_data(char command) {
  switch(command) {
    case '1':
      reset_lights();
      set_color_section_one(255, 255);  // Reset lights, turn on section one
      delay(1000);
    break;

    case '2':
      reset_lights();
      set_color_section_two(255, 255);  // Reset lights, turn on section two
      delay(1000);
    break;

    case 'L':
      let_there_be_light(); // Turn on all lights
    break;
    
    case 'P': // Party mode, light different sections at different colours/intervals
      {
      const int times_to_party = 3;

      party_mode(times_to_party);
    }
    break;

    case 'T': // Toggle TV
      toggle_tv();
    break;

    case 'S': // Toggle string lights on or off
      toggle_string_lights();
    break;

    case 'R': // Reset all Lights
      reset_lights();
      
    break;
  }
}

void toggle_tv() {
  if (tvOn) {
    clear_OLED();
    tvOn = false;
  } else {
    tvOn = true;
  }
}

// Displays what data came in
void show_new_data() {
 if (isNewData == true) {
  Serial.print("This just in:  ");
  Serial.println(receivedChar);
  isNewData = false;
 }
}

// Lights up entire apartment
void let_there_be_light() {
  set_color_section_one(255, 255);
  set_color_section_two(255, 255);
}

void party_mode(const int times_to_party) {
  for(int i = 0; i < times_to_party; i++) {
    set_color_section_one(255, 0);  // red
    delay(500);
    set_color_section_two(0, 255);  // green
    delay(500);

    set_color_section_one(0, 255);  // green
    delay(5000);
    set_color_section_two(255, 0);  // red
    delay(500);
  }
}

void reset_lights() {
  set_color_section_one(0, 0);
  set_color_section_two(0, 0);
}

void toggle_string_lights() {
  if (stringLightsOn) {
    digitalWrite(string_lights, LOW);
    stringLightsOn = false;
  } else {
    digitalWrite(string_lights, HIGH);
    stringLightsOn = true;
  }
}

// Sets the color combination on the RG-LEDs
// Apartment divided into two "sections" for proof-of-concept
void set_color_section_one(int red, int green) {
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
  #endif
  
  analogWrite(section_one_red, red);
  analogWrite(section_one_green, green);
}

void set_color_section_two(int red, int green) {
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
  #endif
  
  analogWrite(section_two_red, red);
  analogWrite(section_two_green, green);
}

/*
 *  ----- Arduino SnowFall ------ 
 *   Created by Andreas Högberg   
 *  
 *  Generates a snowy winters day onto
 *   a 4-pin oled screen connected to
 *        an Arduino/Genuino Uno
 */

// Each flake has a shape value, based on that it draws a different shape
void DrawFlake(Flake flake) {
  // Small flake, 1x1
  if (flake.shape < 7){
    oled.drawPixel(flake.xPos, flake.yPos);
  // Medium flake, 2x2
  } else if (flake.shape >= 8 && flake.shape <= 15) {
    oled.drawPixel(flake.xPos, flake.yPos);
    oled.drawPixel(flake.xPos + 1, flake.yPos);
    oled.drawPixel(flake.xPos, flake.yPos + 1);
    oled.drawPixel(flake.xPos + 1, flake.yPos + 1);
  // Large flake, star-shaped
  } else {
    oled.drawPixel(flake.xPos, flake.yPos - 2);
    oled.drawPixel(flake.xPos, flake.yPos - 1);
    oled.drawPixel(flake.xPos, flake.yPos);
    oled.drawPixel(flake.xPos, flake.yPos + 1);
    oled.drawPixel(flake.xPos, flake.yPos + 2);

    oled.drawPixel(flake.xPos-1, flake.yPos - 1);
    oled.drawPixel(flake.xPos-1, flake.yPos);
    oled.drawPixel(flake.xPos-1, flake.yPos + 1);

    oled.drawPixel(flake.xPos+1, flake.yPos - 1);
    oled.drawPixel(flake.xPos+1, flake.yPos);
    oled.drawPixel(flake.xPos+1, flake.yPos + 1);
  }
}

// Generates a flake at a random xPos and a random yPos(Offscreen)
Flake GenerateFlake() {
  Flake flake;
  flake.xPos = random(1, 127);
  flake.yPos = random(-50, -1);

  // Sets the shape and fall speed (bigger = faster)
  flake.shape = random(1, 16);
  if (flake.shape < 7){
    flake.vel = 0.2;
  } else if (flake.shape >= 8 && flake.shape <= 15) {
    flake.vel = 0.4;
  } else {
    flake.vel = 0.6;
  }

  return flake;
}


// Fill the array with snowflakes
void screen_setup() { 
  for (int i = 0; i < amount_of_flakes; i++) { 
    snow[i] = GenerateFlake();
  }

  randomSeed(analogRead(0));
}

// For each frame, move each flake down, then draw each flake
void screen_loop() {
  do {
    for (int i = 0; i < amount_of_flakes; i++) {
      snow[i].yPos += snow[i].vel;

      // Reset if offscreen
      if (snow[i].yPos > 64) {
        snow[i] = GenerateFlake();
      }

      DrawFlake(snow[i]);
    }
    
  } while (oled.nextPage());
}

void clear_OLED(){
    oled.firstPage();  
    do {
    } while( oled.nextPage() );
}
